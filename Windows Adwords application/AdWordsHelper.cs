﻿using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201603;
using Google.Api.Ads.Common.Util.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Class to comunicate with google adwords
    /// Keeps user and window singleton instances
    /// Only acces to this class, is to window instance to download Raport for client
    /// </summary>
    public class AdWordsHelper
    {
        #region user_singleton
        /// <summary>
        /// Singleton user instance
        /// </summary>
        private static AdWordsUser user;

        /// <summary>
        /// User singleton access field
        /// </summary>
        public AdWordsUser User 
        { 
            get 
            {
                if (user == null)
                    user = new AdWordsUser();
                return user;
            } 
        }
        #endregion

        #region window_singleton
        /// <summary>
        /// Window instance
        /// </summary>
        private static AdWordsHelper instance;

        /// <summary>
        /// Winddow singleton instance access
        /// </summary>
        public static AdWordsHelper Instance
        {
            get
            {
                if (instance == null)
                    instance = new AdWordsHelper();
                return instance;
            }
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private AdWordsHelper() { }
        #endregion

        /// <summary>
        /// Create new user to singleton after reads data from Account info
        /// </summary>
        /// <param name="clientEmail"></param>
        private void createUser(string recordId)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();

            headers.Add("AuthorizationMethod", "OAuth2");
            headers.Add("UserAgent", AccountInfo.Instance.AdWordsApplicationName);
            headers.Add("ClientCustomerId", recordId);
            headers.Add("OAuth2RefreshToken", AccountInfo.Instance.AdWordsApplicationToken);
            headers.Add("DeveloperToken", AccountInfo.Instance.AdWordsToken);
            headers.Add("OAuth2ClientId", AccountInfo.Instance.OAuth2Id);
            headers.Add("OAuth2ClientSecret", AccountInfo.Instance.OAuth2Secret);
  
            user = new AdWordsUser(headers);
        }

        /// <summary>
        /// Create user for report download, and download by in given DataRange
        /// </summary>
        /// <param name="memoryCell"></param>
        public void downloadReports(MemoryCell memoryCell, ReportDefinitionDateRangeType reportDefinitionDateRangeType)
        {
            createUser(memoryCell.ClientNumber);
            ReportDefinition definition = new ReportDefinition()
            {
                reportName = "CRITERIA_PERFORMANCE_REPORT",
                reportType = ReportDefinitionReportType.CRITERIA_PERFORMANCE_REPORT,
                downloadFormat = DownloadFormat.CSVFOREXCEL,
                dateRangeType = reportDefinitionDateRangeType,

                selector = new Selector()
                {
                    fields = new string[] {"CampaignId", "AdGroupId", "Id", "CriteriaType", "Criteria",
              "FinalUrls", "Clicks", "Impressions", "Cost"},
                    predicates = new Predicate[] {
            Predicate.In("Status", new string[] {"ENABLED", "PAUSED"})
          }
                },
            };

            (user.Config as AdWordsAppConfig).IncludeZeroImpressions = true;

            string data = DateTime.Now.ToString().Replace(':','-');
            
            string filePath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + memoryCell.ClientNumber + Path.DirectorySeparatorChar + memoryCell.ClientNumber + " " + data + ".csv";

            try
            {
                ReportUtilities utilities = new ReportUtilities(user, "v201603", definition);
                using (ReportResponse response = utilities.GetResponse())
                {
                    response.Save(filePath);
                }

                memoryCell.ReportDate = DateTime.Now.ToString();
                DataKeeper.Instance.Save();
            }
            catch (Exception e)
            {
                MessageBox.Show("Problem z pobraniem raportu dla "+ memoryCell.ClientNumber,"Błąd pobierania",MessageBoxButton.OK);
            }
        }
    }
}
