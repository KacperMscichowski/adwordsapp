﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Window to Add and Clone records
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {
        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static ClientWindow instance;

            /// <summary>
            /// Instance access field
            /// </summary>
            public static ClientWindow Instance
            {
                get
                {
                    if (instance == null)
                        instance = new ClientWindow();
                    return instance;
                }
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            private ClientWindow()
            {
                InitializeComponent();
            }

            /// <summary>
            /// Custom Show method
            /// </summary>
            public void show()
            {
                instance.SetFields(new MemoryCell());
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 120;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }

            /// <summary>
            /// Custom Show method used to copy data
            /// </summary>
            public void show(MemoryCell memoryCell)
            {
                instance.SetFields(memoryCell);
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 120;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }
        #endregion

        /// <summary>
        /// Sets fields from given memory cell
        /// Always clear and initialize variables
        /// </summary>
        private void SetFields(MemoryCell memoryCell)
        {
            adwords.Text = memoryCell.ClientNumber.ToString();
            email.Text = memoryCell.ClientEmail;
            subject.Text = memoryCell.MailSubject;
            content.Text = memoryCell.HtmlContent;
        }

        /// <summary>
        /// Check for data
        ///     If data is valid Adds record to record list
        ///     And closes window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(adwords.Text))
            {
                MessageBox.Show("Podany numer jest już w bazie");
                return;
            }
            Directory.CreateDirectory(adwords.Text);
            MemoryCell memoryCell = new MemoryCell();
            memoryCell.ClientNumber = adwords.Text;
            memoryCell.ClientEmail = email.Text;
            memoryCell.MailSubject = subject.Text;
            memoryCell.HtmlContent = content.Text;
            DataKeeper.Instance.AddToList(memoryCell);
            DataKeeper.Instance.Save();
            MainWindow.Instance.acctualiseLstView();
            this.Close();
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Clear instance when window is closed, just to keep away from errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
