﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Store save and load EmailHistory
    /// </summary>
    public class History
    {
        /// <summary>
        /// List to keep data in memory serialized deserialized, in all way
        /// </summary>
        public List<HistoryCell> data;

        /// <summary>
        /// filename to save and load data
        /// </summary>
        private static string fileName = "history";
        
        /// <summary>
        /// Class singleton instance
        /// </summary>
        private static History instance;

        /// <summary>
        /// Class singleton instance field
        /// </summary>
        public static History Instance
        {
            get
            {
                if (instance == null)
                    instance = new History();
                return instance;
            }
        }

        /// <summary>
        /// Add Record to list
        /// </summary>
        /// <param name="memoryCell"></param>
        public void AddToList(MemoryCell memoryCell, DateTime date)
        {
            HistoryCell HC = new HistoryCell();
            HC.ClientEmail = memoryCell.ClientEmail;
            HC.ClientNumber = memoryCell.ClientNumber;
            HC.HtmlContent = memoryCell.HtmlContent;
            HC.MailSubject = memoryCell.MailSubject;
            HC.Sended = date;

            string[] files = Directory.GetFiles(memoryCell.ClientNumber);

            foreach (string s in files)
            {
                HC.FileList += s + " ";
            }

            data.Add(HC);
            Save();
        }


        /// <summary>
        /// Private constructor
        /// </summary>
        private History() 
        {
            data = new List<HistoryCell>();
            Load();
        }

        /// <summary>
        /// Save data to binarysed JSON file
        /// </summary>
        private void Save()
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.CreateNew)))
            {
                string json = JsonConvert.SerializeObject(data);
                writer.Write(json);
            }
        }

        /// <summary>
        /// Load data from binarysed JSON file
        /// </summary>
        private void Load()
        {
            if (!File.Exists(fileName))
                return;
            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                string json = "";
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    json += reader.ReadString();
                }
                data = JsonConvert.DeserializeObject<List<HistoryCell>>(json);
            }
        }
    }

    /// <summary>
    /// MemoryCell for mail history, clears data al construction
    /// </summary>
    public class HistoryCell
    {
        /// <summary>
        /// Client access acount number
        /// </summary>
        public string ClientNumber { get; set; }

        /// <summary>
        /// Date Of email sended
        /// </summary>
        public DateTime Sended { get; set; }

        /// <summary>
        /// Address to send email
        /// </summary>
        public string ClientEmail { get; set; }

        /// <summary>
        /// Email subject
        /// </summary>
        public string MailSubject { get; set; }

        /// <summary>
        /// Email content written in html
        /// </summary>
        public string HtmlContent { get; set; }

        /// <summary>
        /// String that keeps all FileList
        /// </summary>
        public string FileList { get; set; }

        /// <summary>
        /// Always clear and initialize variables
        /// </summary>
        public HistoryCell()
        {
            ClientNumber = "";
            ClientEmail = "";
            Sended = DateTime.MinValue;
            MailSubject = "";
            HtmlContent = "";
            FileList = "";
        }
    }
}
