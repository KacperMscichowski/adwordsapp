﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Window to Modify records
    /// Interaction logic for EditClient.xaml
    /// </summary>
    public partial class EditClient : Window
    {
        /// <summary>
        /// Data given in show method,
        ///     used becouse folder and other inside data are used in program
        /// </summary>
        private MemoryCell memoryCell;

        /// <summary>
        /// Data given in show method,
        ///     is necessary to modify data in list
        /// </summary>
        private int index;

        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static EditClient instance;

            /// <summary>
            /// Instance access field
            /// </summary>
            public static EditClient Instance
            {
                get
                {
                    if (instance == null)
                        instance = new EditClient();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method
            /// </summary>
            public void show(MemoryCell memoryCell,int index)
            {
                instance.SetFields(memoryCell);
                this.memoryCell = memoryCell;
                this.index = index;
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 120;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            public EditClient()
            {
                InitializeComponent();
            }
        #endregion

        /// <summary>
        /// Sets fields from given memory cell
        /// Always clear and initialize variables
        /// </summary>
        private void SetFields(MemoryCell memoryCell)
        {
            adwords.Text = memoryCell.ClientNumber.ToString();
            email.Text = memoryCell.ClientEmail;
            subject.Text = memoryCell.MailSubject;
            content.Text = memoryCell.HtmlContent;
        }

        /// <summary>
        /// Check for data
        ///     If data is valid Adds record to record list
        ///     And closes window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void save(object sender, RoutedEventArgs e)
        {
            if(Directory.Exists(adwords.Text) && adwords.Text != memoryCell.ClientNumber)
            {
                MessageBox.Show("Podany numer jest już w bazie");
                return;
            }
            if (adwords.Text != memoryCell.ClientNumber)
                Directory.Move(memoryCell.ClientNumber, adwords.Text);
            //Directory.CreateDirectory(adwords.Text);
            memoryCell.ClientNumber = adwords.Text;
            memoryCell.ClientEmail = email.Text;
            memoryCell.MailSubject = subject.Text;
            memoryCell.HtmlContent = content.Text;
            DataKeeper.Instance.EditOnList(memoryCell,this.index);
            DataKeeper.Instance.Save();
            MainWindow.Instance.acctualiseLstView();
            this.Close();
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Clear instance when window is closed, just to keep away from errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
