﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Class to keep Data
    /// Save and load them
    /// </summary>
    public class DataKeeper
    {
        /// <summary>
        /// filename to save and load data
        /// </summary>
        private static string fileName = "data";

        /// <summary>
        /// Class singleton instance
        /// </summary>
        private static DataKeeper instance;

        /// <summary>
        /// Class singleton instance field
        /// </summary>
        public static DataKeeper Instance
        {
            get
            {
                if (instance == null)
                    instance = new DataKeeper();
                return instance;
            }
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private DataKeeper() { }

        /// <summary>
        /// List to keep data in memory serialized deserialized, in all way
        /// </summary>
        public List<MemoryCell> RecordsList = new List<MemoryCell>();

        /// <summary>
        /// Add Record to list
        /// </summary>
        /// <param name="memoryCell"></param>
        public void AddToList(MemoryCell memoryCell)
        {
            RecordsList.Add(memoryCell);
        }

        /// <summary>
        /// Replace MemoryCell (Record) in given index
        /// </summary>
        /// <param name="memoryCell"></param>
        /// <param name="index"></param>
        public void EditOnList(MemoryCell memoryCell,int index)
        {
            if (index + 1 != RecordsList.Count)
                RecordsList.Insert(index + 1, memoryCell);
            else
                RecordsList.Add(memoryCell);
            RecordsList.RemoveAt(index);
        }

        /// <summary>
        /// Save data to binarysed JSON file
        /// </summary>
        public void Save()
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.CreateNew)))
            {
                string json = JsonConvert.SerializeObject(RecordsList);
                writer.Write(json);
            }
        }

        /// <summary>
        /// Load data from binarysed JSON file
        /// </summary>
        public void Load()
        {
            if (!File.Exists(fileName))
                return;
            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                string json = "";
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    json += reader.ReadString();
                }
                RecordsList = JsonConvert.DeserializeObject<List<MemoryCell>>(json);
            }
        }

    }
}
