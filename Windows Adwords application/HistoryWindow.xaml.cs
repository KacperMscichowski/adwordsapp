﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Interaction logic for HistoryWindow.xaml
    /// </summary>
    public partial class HistoryWindow : Window
    {
        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static HistoryWindow instance;
        
            /// <summary>
            /// Instance access field
            /// </summary>
            public static HistoryWindow Instance
            {
                get
                {
                    if (instance == null)
                        instance = new HistoryWindow();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method, keeps everything cool
            /// </summary>
            public void show()
            {
                instance.Show();
                instance.Activate();
                acctualiseLstView();
                instance.Left = Application.Current.MainWindow.Left + 30;
                instance.Top = Application.Current.MainWindow.Top + 30;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            private HistoryWindow()
            {
                InitializeComponent();
            }
        #endregion

        /// <summary>
        /// Method that refresh data on window
        /// </summary>
        public void acctualiseLstView()
        {
            rekordy.Items.Clear();

            foreach (HistoryCell r in History.Instance.data)
                rekordy.Items.Add(r);
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close(object sender, RoutedEventArgs e)
        {
            instance.Close();
        }

        /// <summary>
        /// Refreshes Window content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Refresh(object sender, RoutedEventArgs e)
        {
            acctualiseLstView();
        }

        /// <summary>
        /// Clears 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
