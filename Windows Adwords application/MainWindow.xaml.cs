﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Init mainwindow read data and shows them
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            instance = this;
            DataKeeper.Instance.Load();
            acctualiseLstView();
        }

        /// <summary>
        /// Method that refresh data on window
        /// </summary>
        public void acctualiseLstView()
        {
            rekordy.Items.Clear();

            foreach (MemoryCell r in DataKeeper.Instance.RecordsList)
                rekordy.Items.Add(r);
        }

        #region singleton
        /// <summary>
        /// Singleton instance
        /// </summary>
        private static MainWindow instance;

        /// <summary>
        /// 
        /// </summary>
        public static MainWindow Instance
        {
            get
            {
                if (instance == null)
                    instance = new MainWindow();
                return instance;
            }
        }

        /// <summary>
        /// Singleton show and put on main screen
        /// </summary>
        public void show()
        {
            instance.Show();
            instance.Activate();
        }
        #endregion

        #region MenuBlock

        /// <summary>
        /// Application option window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Options(object sender, RoutedEventArgs e)
        {
            ApplicationOption.Instance.show();
        }

        /// <summary>
        /// ExitApplication option
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Info Window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Info(object sender, RoutedEventArgs e)
        {
            InformationWindow.Instance.show();
        }

        /// <summary>
        /// Add record window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add(object sender, RoutedEventArgs e)
        {
            ClientWindow.Instance.show();
        }

        /// <summary>
        /// Edit rekord window, shows only when record on list is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Edit(object sender, RoutedEventArgs e)
        {
            MemoryCell r = (MemoryCell)rekordy.SelectedItem;
            if (r != null)
                EditClient.Instance.show(r, rekordy.SelectedIndex);
        }

        /// <summary>
        /// Remove selected record from list, only when item is selected. And only after messagebox that can cancel operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove(object sender, RoutedEventArgs e)
        {
            MemoryCell r = (MemoryCell)rekordy.SelectedItem;
            if (r != null)
            {
                if (MessageBox.Show(Application.Current.MainWindow,"Czy na pewno chcesz usunąć rekord o numerze " + r.ClientNumber, "Potwierdzenie", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    DataKeeper.Instance.RecordsList.RemoveAt(rekordy.SelectedIndex);
                    DataKeeper.Instance.Save();
                    Directory.Delete(r.ClientNumber);
                }
                acctualiseLstView();
            }
        }

        /// <summary>
        /// Copy selected record to add record window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Copy(object sender, RoutedEventArgs e)
        {
            MemoryCell r = (MemoryCell)rekordy.SelectedItem;
            if (r != null)
                ClientWindow.Instance.show(r);
        }

        /// <summary>
        /// Sended email history window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void History(object sender, RoutedEventArgs e)
        {
            HistoryWindow.Instance.show();
        }

        /// <summary>
        /// Open directory of application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Explorer(object sender, RoutedEventArgs e)
        {
            Process.Start(Directory.GetCurrentDirectory());
        }
        #endregion

        /// <summary>
        /// Add files to selected index directory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addFiles(object sender, RoutedEventArgs e)
        {

            Button b = sender as Button;
            MemoryCell memoryCell = b.CommandParameter as MemoryCell;

            string path = Directory.GetCurrentDirectory() + @"\" + memoryCell.ClientNumber;


            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            OpenFileDialog openFileDialog = new OpenFileDialog();
            
            if (openFileDialog.ShowDialog(this) == true)
            {
                try
                {
                    string file =  path +@"\"+ openFileDialog.FileName.Split('\\').ElementAt(openFileDialog.FileName.Split('\\').Length-1);
                    File.Copy(openFileDialog.FileName,file);
                    File.SetAttributes(file, FileAttributes.Normal);
                }
                catch (Exception)
                {
                    MessageBox.Show("Error");
                }
            }
        }

        /// <summary>
        /// Open send e-mail window, this window sends mail with every file in record directory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void send(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            MemoryCell memoryCell = b.CommandParameter as MemoryCell;
            EmailWindow.Instance.show(memoryCell);
        }

        /// <summary>
        /// !! Not tested !!
        /// This method download record report from AdWords serwer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DownloadReport(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            MemoryCell memoryCell = b.CommandParameter as MemoryCell;
            Download.Instance.show(memoryCell);
        }

        /// <summary>
        /// Shut down all aplication when this windows close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        

        
    }
}
