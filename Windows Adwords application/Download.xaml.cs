﻿using Google.Api.Ads.AdWords.v201603;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Interaction logic for Download.xaml
    /// </summary>
    public partial class Download : Window
    {
        MemoryCell memoryCell;

        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static Download instance;

            /// <summary>
            /// Instance access field
            /// </summary>
            public static Download Instance
            {
                get
                {
                    if (instance == null)
                        instance = new Download();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method
            /// </summary>
            public void show(MemoryCell memorycell)
            {
                this.memoryCell = memorycell;
                SetFields();
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 80;
                instance.Top = Application.Current.MainWindow.Top + 80;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            public Download()
            {
                InitializeComponent();
            }
        #endregion

        /// <summary>
        /// Sets fields static
        /// Always clear and initialize variables
        /// </summary>
        private void SetFields()
        {
            Options.Items.Clear();
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Dziś";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Wczoraj";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatni tydzień roboczy";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatnie 7 dni";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatni tydzień";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatnie 14 dni";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatnie 30 dni";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ostatni miesiąc";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Ten miesiąc";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Od poniedzialku do dziś";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Od niedzieli do dziś";
            Options.Items.Add(comboBoxItem);
            comboBoxItem = new ComboBoxItem();
            comboBoxItem.Content = "Cały okres";
            Options.Items.Add(comboBoxItem);

        }

        /// <summary>
        /// Download raport, from selected in combobox DataRange
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void download(object sender, RoutedEventArgs e)
        {
            AdWordsHelper.Instance.downloadReports(memoryCell, getDataRange());
            instance.Close();
        }

        /// <summary>
        /// Change value from ComboBox to Date Range
        /// </summary>
        /// <returns></returns>
        private ReportDefinitionDateRangeType getDataRange()
        {
            string s = Options.Text;
            if (s == "Dziś")
                return ReportDefinitionDateRangeType.TODAY;
            if (s == "Wczoraj")
                return ReportDefinitionDateRangeType.YESTERDAY;
            if (s == "Ostatni tydzień roboczy")
                return ReportDefinitionDateRangeType.LAST_BUSINESS_WEEK;
            if (s == "Ostatnie 7 dni")
                return ReportDefinitionDateRangeType.LAST_7_DAYS;
            if (s == "Ostatni tydzień")
                return ReportDefinitionDateRangeType.LAST_WEEK;
            if (s == "Ostatnie 14 dni")
                return ReportDefinitionDateRangeType.LAST_14_DAYS;
            if (s == "Ostatnie 30 dni")
                return ReportDefinitionDateRangeType.LAST_30_DAYS;
            if (s == "Ostatni miesiąc")
                return ReportDefinitionDateRangeType.LAST_MONTH;
            if (s == "Ten miesiąc")
                return ReportDefinitionDateRangeType.THIS_MONTH;
            if (s == "Od poniedzialku do dziś")
                return ReportDefinitionDateRangeType.THIS_WEEK_MON_TODAY;
            if (s == "Od niedzieli do dziś")
                return ReportDefinitionDateRangeType.THIS_WEEK_SUN_TODAY;
            if (s == "Cały okres")
                return ReportDefinitionDateRangeType.ALL_TIME;

            return ReportDefinitionDateRangeType.TODAY;
        }

        /// <summary>
        /// Button to close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close(object sender, RoutedEventArgs e)
        {
            instance.Close();
        }

        /// <summary>
        /// Clears window instance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
