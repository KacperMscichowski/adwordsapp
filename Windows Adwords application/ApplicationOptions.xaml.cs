﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Singleton based window for user account data.
    /// In this window user should add data for email and adwords account
    /// Interaction logic for AplicationOptions.xaml
    /// </summary>
    public partial class ApplicationOption : Window
    {
        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static ApplicationOption instance;
        
            /// <summary>
            /// Instance access field
            /// </summary>
            public static ApplicationOption Instance
            {
                get
                {
                    if (instance == null)
                        instance = new ApplicationOption();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method, keeps everything cool
            /// </summary>
            public void show()
            {
                SetFields();
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 70;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            private ApplicationOption()
            {
                InitializeComponent();
            }
        #endregion

        /// <summary>
        /// Sets fields for this readed from data
        /// Always clear and initialize variables
        /// </summary>
        private void SetFields()
        {
            token.Text = AccountInfo.Instance.AdWordsToken;
            email.Text = AccountInfo.Instance.emailAccount;
            password.Password = AccountInfo.Instance.emailPassword;
            port.Text = AccountInfo.Instance.port;
            host.Text = AccountInfo.Instance.smtpHost;
            OAuth2ClientId.Text = AccountInfo.Instance.OAuth2Id;
            OAuth2ClientSecret.Text = AccountInfo.Instance.OAuth2Secret;
            appToken.Text = AccountInfo.Instance.AdWordsApplicationToken;
            OAuth2Name.Text = AccountInfo.Instance.AdWordsApplicationName;
        }

        /// <summary>
        /// Check for data
        ///     If data is valid save data to Account info instance and to file;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save(object sender, RoutedEventArgs e)
        {
            if(email.Text == string.Empty ||
                port.Text == string.Empty ||
                password.Password == string.Empty ||
                host.Text == string.Empty)
            {
                MessageBox.Show("Jedno lub więcej pól, nie jest wypełnionych. Aplikacja nie będzie działać poprawnie. Aplikacja nie zapisze aktualnych danych.", "Puste pole", MessageBoxButton.OK);
                return;
            }
            AccountInfo.Instance.AdWordsToken = token.Text;
            AccountInfo.Instance.emailAccount = email.Text;
            AccountInfo.Instance.emailPassword = password.Password;
            AccountInfo.Instance.port = port.Text;
            AccountInfo.Instance.smtpHost = host.Text;
            AccountInfo.Instance.OAuth2Id = OAuth2ClientId.Text;
            AccountInfo.Instance.OAuth2Secret = OAuth2ClientSecret.Text;
            AccountInfo.Instance.AdWordsApplicationToken = appToken.Text;
            AccountInfo.Instance.AdWordsApplicationName = OAuth2Name.Text;

            AccountInfo.Instance.Save();
            
            this.Close();
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseIt(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Clear instance when window is closed, just to keep away from errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }


    }
}
