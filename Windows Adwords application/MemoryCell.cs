﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Windows_Adwords_application
{
    /// <summary>
    /// It is onlu cell for records list keeps data
    /// Data are serialized and deserialized by JSON, 
    ///     i changed AccountInfo class once and project readed data, 
    ///     so if you need to add or remove field, you can try.
    ///         BUT if you change or remove field after running program you will lost data from that "column"
    /// </summary>
    public class MemoryCell
    {
        /// <summary>
        /// Raport numbers
        /// #Not supported i didn't saw a point of that
        /// </summary>
        public string RaportNumbers { get; set; }

        /// <summary>
        /// Client access acount number
        /// </summary>
        public string ClientNumber { get; set; }

        /// <summary>
        /// Date of last repord download
        /// </summary>
        public string ReportDate { get; set; }

        /// <summary>
        /// Address to send email
        /// </summary>
        public string ClientEmail { get; set; }

        /// <summary>
        /// Email subject
        /// </summary>
        public string MailSubject { get; set; }

        /// <summary>
        /// Email content written in html
        /// </summary>
        public string HtmlContent { get; set; }

        /// <summary>
        /// Always clear and initialize variables
        /// </summary>
        public MemoryCell()
        {
            RaportNumbers = "";
            ClientNumber = "";
            ReportDate = "";
            ClientEmail = "";
            MailSubject = "";
            HtmlContent = "";

        }
    }
}
