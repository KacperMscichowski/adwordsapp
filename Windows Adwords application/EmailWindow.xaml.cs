﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Window used to demonstrate email, and send them
    /// Interaction logic for EmailWindow.xaml
    /// </summary>
    public partial class EmailWindow : Window
    {
        /// <summary>
        /// Record to send email to
        /// </summary>
        private MemoryCell sendind;

        #region Singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static EmailWindow instance;

            /// <summary>
            /// Instance access field
            /// </summary>
            public static EmailWindow Instance
            {
                get
                {
                    if (instance == null)
                        instance = new EmailWindow();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method
            /// </summary>
            public void show(MemoryCell memoryCell)
            {
                instance.SetFields(memoryCell);
                sendind = memoryCell;
                instance.Show();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 140;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            private EmailWindow()
            {
                InitializeComponent();
            }
        #endregion
        /// <summary>
        /// Sets fields from given memory cell
        /// Always clear and initialize variables
        /// </summary>
        private void SetFields(MemoryCell memoryCell)
        {
            adwords.Content = memoryCell.ClientNumber.ToString();
            email.Content = memoryCell.ClientEmail;
            subject.Content = memoryCell.MailSubject;
            content.Text = memoryCell.HtmlContent;
            foreach(string s in Directory.GetFiles(memoryCell.ClientNumber))
            {
                files.Content += s.Split('\\').ElementAt(s.Split('\\').Length-1) + "\r\n";
            }
        }


        /// <summary>
        /// Send raport to record
        ///     Data to record was given in show method
        ///     User data to send email is taked from AccountInfo class as is from file
        ///     Method includes send files
        ///     Content in email can be Html written
        /// Also save email when sended
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void send(object sender, RoutedEventArgs e)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(AccountInfo.Instance.emailAccount, AccountInfo.Instance.emailPassword);
                try
                {
                    SmtpServer.Port = int.Parse(AccountInfo.Instance.port);
                }
                catch (Exception) { MessageBox.Show("Port nie jest Liczbą"); }
                SmtpServer.Host = AccountInfo.Instance.smtpHost;
                SmtpServer.EnableSsl = true;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(AccountInfo.Instance.emailAccount, "VisualPromo");
                mail.To.Add(new MailAddress(sendind.ClientEmail));
                mail.Subject = sendind.MailSubject;
                mail.Body = sendind.HtmlContent;

                string[] files = Directory.GetFiles(sendind.ClientNumber);

                foreach (string s in files)
                {
                    Attachment attachment = new Attachment(s, MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(s);
                    disposition.ModificationDate = File.GetLastWriteTime(s);
                    disposition.ReadDate = File.GetLastAccessTime(s);
                    disposition.FileName = Path.GetFileName(s);
                    disposition.Size = new FileInfo(s).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    mail.Attachments.Add(attachment);
                }

                mail.IsBodyHtml = true;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                SmtpServer.Send(mail);

                History.Instance.AddToList(sendind, DateTime.Now);
                Task.Run(() =>
                {
                    moveFiles();
                });
                
            }
            catch (Exception) { MessageBox.Show("Sending email error ocured"); }
            this.Close();
        }

        /// <summary>
        /// Move files aftem email is sended
        /// </summary>
        private void moveFiles()
        {
            string directory = @Directory.GetCurrentDirectory() + "\\" + sendind.ClientNumber + "\\Wysłane";
            if(!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            string[] files = Directory.GetFiles(sendind.ClientNumber);
            Thread.Sleep(5000);
            try
            {
                foreach (string s in files)
                {
                    string file = @Directory.GetCurrentDirectory() + @"\" + @s;
                    FileInfo fi = new FileInfo(file); 
                    if (fi.IsReadOnly) 
                          fi.IsReadOnly = false;
                    fi.MoveTo(directory + "\\" + Path.GetFileName(s));
                }
            }
            catch (IOException) { moveFiles(); }
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Clear instance when window is closed, just to keep away from errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
