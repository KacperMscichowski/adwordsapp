﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Keeps, loads, saves user data in instance
    /// Free access
    /// </summary>
    public class AccountInfo
    {
        /// <summary>
        /// Email to send file from 
        /// </summary>
        public string emailAccount { get; set; }

        /// <summary>
        /// Email to send file from password
        /// </summary>
        public string emailPassword { get; set; }
        
        /// <summary>
        /// Port of email smtp port
        ///     examle gmail 587
        /// </summary>
        public string port { get; set; }

        /// <summary>
        /// Host of smtp email account
        ///     Example gmail smtp.gmail.com
        /// </summary>
        public string smtpHost { get; set; }

        /// <summary>
        /// Menager Adwords token
        /// </summary>
        public string AdWordsToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OAuth2Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OAuth2Secret { get; set; }


        /// <summary>
        /// Token of application is diffrent that menager account 
        /// is created later
        /// </summary>
        public string AdWordsApplicationToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdWordsApplicationName { get; set; }

        /// <summary>
        /// Account file data file
        /// </summary>
        private string fileName = "account";

        #region singleton
            /// <summary>
            /// Singleton field
            /// </summary>
            private static AccountInfo instance;

            /// <summary>
            /// Singleton instance access field
            /// </summary>
            public static AccountInfo Instance
            {
                get
                {
                    if (instance == null)
                    {
                        instance = new AccountInfo();
                        instance.Load();
                    }
                    return instance;
                }
            }

            /// <summary>
            /// Always clear and initialize variables
            /// </summary>
            private AccountInfo() 
            {
                instance = this;
                emailAccount = "";
                emailPassword = "";
                port = "";
                AdWordsToken = "";
                smtpHost = "";
                OAuth2Id = "";
                OAuth2Secret = "";
                AdWordsApplicationToken = "";
                AdWordsApplicationName = "";
            }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void Save()
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.CreateNew)))
            {
                string json = JsonConvert.SerializeObject(this);
                writer.Write(json);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Load()
        {
            if (!File.Exists(fileName))
                return;

            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var reader = new BinaryReader(fs, Encoding.Default)) 
            {
                string json = "";
                while (reader.BaseStream.Position != reader.BaseStream.Length)
                {
                    json += reader.ReadString();
                }
                AccountInfo tmp = new AccountInfo();
                tmp = JsonConvert.DeserializeObject<AccountInfo>(json);
                instance.AdWordsToken = tmp.AdWordsToken;
                instance.emailAccount = tmp.emailAccount;
                instance.emailPassword = tmp.emailPassword;
                instance.port = tmp.port;
                instance.smtpHost = tmp.smtpHost;
                instance.AdWordsApplicationName = tmp.AdWordsApplicationName;
            }
        }
    }
}
