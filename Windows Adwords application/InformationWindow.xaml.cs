﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windows_Adwords_application
{
    /// <summary>
    /// Custom information info not much
    /// Interaction logic for InformationWindow.xaml
    /// </summary>
    public partial class InformationWindow : Window
    {
        #region singleton
            /// <summary>
            /// Window instance
            /// </summary>
            private static InformationWindow instance;

            /// <summary>
            /// Instance access field
            /// </summary>
            public static InformationWindow Instance
            {
                get
                {
                    if (instance == null)
                        instance = new InformationWindow();
                    return instance;
                }
            }

            /// <summary>
            /// Custom Show method
            /// </summary>
            public void show()
            {
                instance.Show();
                instance.readInfo();
                instance.Activate();
                instance.Left = Application.Current.MainWindow.Left + 120;
                instance.Top = Application.Current.MainWindow.Top + 10;
            }

            /// <summary>
            /// Window constructor
            /// </summary>
            private InformationWindow()
            {
                InitializeComponent();
            }
        #endregion

        /// <summary>
        /// Read information from InfoFile and displays them
        /// </summary>
        private void readInfo()
        {
            string path = "Info";
            if(!File.Exists(path))
                this.Close();
            StreamReader reader = new StreamReader(path);
            info.Content = reader.ReadToEnd();
        }

        /// <summary>
        /// Close window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Clear instance when window is closed, just to keep away from errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, EventArgs e)
        {
            instance = null;
        }
    }
}
